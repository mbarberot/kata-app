#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER kata_app_user WITH PASSWORD 'test';
    CREATE DATABASE kata_app WITH OWNER kata_app_user;
    GRANT ALL PRIVILEGES ON DATABASE kata_app TO kata_app_user;
EOSQL
