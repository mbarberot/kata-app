\connect kata_app kata_app_user

CREATE TABLE fizzbuzz (
    id      SERIAL PRIMARY KEY  NOT NULL,
    value   INT                 NOT NULL
);

INSERT INTO fizzbuzz ("value") VALUES (1);
INSERT INTO fizzbuzz ("value") VALUES (2);
INSERT INTO fizzbuzz ("value") VALUES (3);
INSERT INTO fizzbuzz ("value") VALUES (4);
INSERT INTO fizzbuzz ("value") VALUES (5);
INSERT INTO fizzbuzz ("value") VALUES (6);
INSERT INTO fizzbuzz ("value") VALUES (7);
INSERT INTO fizzbuzz ("value") VALUES (8);
INSERT INTO fizzbuzz ("value") VALUES (9);
INSERT INTO fizzbuzz ("value") VALUES (10);
INSERT INTO fizzbuzz ("value") VALUES (11);
INSERT INTO fizzbuzz ("value") VALUES (12);
INSERT INTO fizzbuzz ("value") VALUES (13);
INSERT INTO fizzbuzz ("value") VALUES (14);
INSERT INTO fizzbuzz ("value") VALUES (15);
INSERT INTO fizzbuzz ("value") VALUES (16);
INSERT INTO fizzbuzz ("value") VALUES (17);
INSERT INTO fizzbuzz ("value") VALUES (18);
INSERT INTO fizzbuzz ("value") VALUES (19);
INSERT INTO fizzbuzz ("value") VALUES (20);
INSERT INTO fizzbuzz ("value") VALUES (21);
INSERT INTO fizzbuzz ("value") VALUES (22);
INSERT INTO fizzbuzz ("value") VALUES (23);
INSERT INTO fizzbuzz ("value") VALUES (24);
INSERT INTO fizzbuzz ("value") VALUES (25);
INSERT INTO fizzbuzz ("value") VALUES (26);
INSERT INTO fizzbuzz ("value") VALUES (27);
INSERT INTO fizzbuzz ("value") VALUES (28);
INSERT INTO fizzbuzz ("value") VALUES (29);
INSERT INTO fizzbuzz ("value") VALUES (30);
INSERT INTO fizzbuzz ("value") VALUES (31);
INSERT INTO fizzbuzz ("value") VALUES (32);
INSERT INTO fizzbuzz ("value") VALUES (33);
INSERT INTO fizzbuzz ("value") VALUES (34);
INSERT INTO fizzbuzz ("value") VALUES (35);
INSERT INTO fizzbuzz ("value") VALUES (36);
INSERT INTO fizzbuzz ("value") VALUES (37);
INSERT INTO fizzbuzz ("value") VALUES (38);
INSERT INTO fizzbuzz ("value") VALUES (39);
INSERT INTO fizzbuzz ("value") VALUES (40);
INSERT INTO fizzbuzz ("value") VALUES (41);
INSERT INTO fizzbuzz ("value") VALUES (42);
INSERT INTO fizzbuzz ("value") VALUES (43);
INSERT INTO fizzbuzz ("value") VALUES (44);
INSERT INTO fizzbuzz ("value") VALUES (45);
INSERT INTO fizzbuzz ("value") VALUES (46);
INSERT INTO fizzbuzz ("value") VALUES (47);
INSERT INTO fizzbuzz ("value") VALUES (48);
INSERT INTO fizzbuzz ("value") VALUES (49);
INSERT INTO fizzbuzz ("value") VALUES (50);
INSERT INTO fizzbuzz ("value") VALUES (51);
INSERT INTO fizzbuzz ("value") VALUES (52);
INSERT INTO fizzbuzz ("value") VALUES (53);
INSERT INTO fizzbuzz ("value") VALUES (54);
INSERT INTO fizzbuzz ("value") VALUES (55);
INSERT INTO fizzbuzz ("value") VALUES (56);
INSERT INTO fizzbuzz ("value") VALUES (57);
INSERT INTO fizzbuzz ("value") VALUES (58);
INSERT INTO fizzbuzz ("value") VALUES (59);
INSERT INTO fizzbuzz ("value") VALUES (60);
INSERT INTO fizzbuzz ("value") VALUES (61);
INSERT INTO fizzbuzz ("value") VALUES (62);
INSERT INTO fizzbuzz ("value") VALUES (63);
INSERT INTO fizzbuzz ("value") VALUES (64);
INSERT INTO fizzbuzz ("value") VALUES (65);
INSERT INTO fizzbuzz ("value") VALUES (66);
INSERT INTO fizzbuzz ("value") VALUES (67);
INSERT INTO fizzbuzz ("value") VALUES (68);
INSERT INTO fizzbuzz ("value") VALUES (69);
INSERT INTO fizzbuzz ("value") VALUES (70);
INSERT INTO fizzbuzz ("value") VALUES (71);
INSERT INTO fizzbuzz ("value") VALUES (72);
INSERT INTO fizzbuzz ("value") VALUES (73);
INSERT INTO fizzbuzz ("value") VALUES (74);
INSERT INTO fizzbuzz ("value") VALUES (75);
INSERT INTO fizzbuzz ("value") VALUES (76);
INSERT INTO fizzbuzz ("value") VALUES (77);
INSERT INTO fizzbuzz ("value") VALUES (78);
INSERT INTO fizzbuzz ("value") VALUES (79);
INSERT INTO fizzbuzz ("value") VALUES (80);
INSERT INTO fizzbuzz ("value") VALUES (81);
INSERT INTO fizzbuzz ("value") VALUES (82);
INSERT INTO fizzbuzz ("value") VALUES (83);
INSERT INTO fizzbuzz ("value") VALUES (84);
INSERT INTO fizzbuzz ("value") VALUES (85);
INSERT INTO fizzbuzz ("value") VALUES (86);
INSERT INTO fizzbuzz ("value") VALUES (87);
INSERT INTO fizzbuzz ("value") VALUES (88);
INSERT INTO fizzbuzz ("value") VALUES (89);
INSERT INTO fizzbuzz ("value") VALUES (90);
INSERT INTO fizzbuzz ("value") VALUES (91);
INSERT INTO fizzbuzz ("value") VALUES (92);
INSERT INTO fizzbuzz ("value") VALUES (93);
INSERT INTO fizzbuzz ("value") VALUES (94);
INSERT INTO fizzbuzz ("value") VALUES (95);
INSERT INTO fizzbuzz ("value") VALUES (96);
INSERT INTO fizzbuzz ("value") VALUES (97);
INSERT INTO fizzbuzz ("value") VALUES (98);
INSERT INTO fizzbuzz ("value") VALUES (99);
INSERT INTO fizzbuzz ("value") VALUES (100);
