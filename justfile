
default:
  just --list

# Reset database
db-init:
	cp fizzbuzz/sql/* docker/postgres/db_scripts/
	podman-compose up -d db adminer
