package app;

import app.api.ApiConfiguration;
import app.api.ApiController;
import app.view.HomePage;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.rendering.template.JavalinJte;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;

public class Startup {

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        JavalinJte.init();
        Javalin.create(config -> {
                   config.plugins.register(ApiConfiguration.openApiPlugin());
                   config.plugins.register(ApiConfiguration.swaggerPlugin());
                   config.plugins.register(ApiConfiguration.redocPlugin());
                   config.staticFiles.add(staticFiles -> {
                       staticFiles.directory = "/assets";
                       staticFiles.hostedPath = "/assets";
                       staticFiles.location = Location.CLASSPATH;
                   });
               })
               .routes(() -> {
                   get("/", HomePage::render);

                   path("/ui", () -> {
                       get("/hello_world", ctx -> ctx.render("ui/hello_world.jte"));
                       get("/fizzbuzz", ctx -> ctx.render("ui/fizzbuzz.jte"));
                   });

                   path("/api", () -> {
                       get("/", ApiController::get);
                   });
               })
               .start(7070);
    }
}
