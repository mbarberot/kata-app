package app.domain;

public record Kata(
    String name
) {

    public String key() {
        return name.toLowerCase().replace(" ", "_");
    }
}
