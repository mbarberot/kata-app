package app.view;

import app.domain.Kata;
import io.javalin.http.Context;

import java.util.List;
import java.util.Map;

public record HomePage(
    List<Kata> katas
) {
    public static void render(Context ctx) {

        HomePage page = new HomePage(
            List.of(
                new Kata("hello world"),
                new Kata("fizzbuzz")
            )
        );

        ctx.render("page/home.jte", Map.of("page", page));
    }
}
