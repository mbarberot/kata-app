package app.api;

import io.javalin.http.Context;
import io.javalin.openapi.HttpMethod;
import io.javalin.openapi.OpenApi;
import io.javalin.openapi.OpenApiContent;
import io.javalin.openapi.OpenApiResponse;

public class ApiController {

    @OpenApi(
        summary = "Get API informations",
        operationId = "getApiInfos",
        path = "/",
        methods = HttpMethod.GET,
        tags = {"API"},
        responses = {
            @OpenApiResponse(status = "200", content = {@OpenApiContent(from = ApiInformations.class)})
        }
    )
    public static void get(Context ctx) {
        ctx.json(new ApiInformations());
    }

    private ApiController() {
    }
}
