package app.api;

import java.util.Collection;
import java.util.List;

public record ApiInformations(String name, String version, Collection<String> resources) {

    public ApiInformations() {
        this(
            "Javalin POC Rest API",
            "v1.0.0",
            List.of(
                "/api"
            )
        );
    }

}
