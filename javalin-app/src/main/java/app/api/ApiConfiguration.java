package app.api;

import io.javalin.openapi.plugin.OpenApiPlugin;
import io.javalin.openapi.plugin.OpenApiPluginConfiguration;
import io.javalin.openapi.plugin.redoc.ReDocConfiguration;
import io.javalin.openapi.plugin.redoc.ReDocPlugin;
import io.javalin.openapi.plugin.swagger.SwaggerConfiguration;
import io.javalin.openapi.plugin.swagger.SwaggerPlugin;
import io.javalin.plugin.Plugin;

public class ApiConfiguration {
    public static Plugin openApiPlugin() {
        return new OpenApiPlugin(
            new OpenApiPluginConfiguration()
                .withDocumentationPath("/openapi")
                .withDefinitionConfiguration((version, definition) -> definition
                    .withOpenApiInfo(openApiInfo -> {
                        openApiInfo.setTitle("Kata API powered by Javalin");
                        openApiInfo.setVersion("v1.0.0");
                    })
                    .withServer(openApiServer -> {
                        openApiServer.setUrl("http://localhost:{port}{basePath}/");
                        openApiServer.addVariable("port", "7070", new String[]{"7070", "8080"}, "Port of the server");
                        openApiServer.addVariable("basePath", "/api", new String[]{"/api"}, "Base path of the server");
                    })
                )
        );
    }

    public static Plugin swaggerPlugin() {
        SwaggerConfiguration configuration = new SwaggerConfiguration();
        configuration.setUiPath("/swagger-ui");
        return new SwaggerPlugin(configuration);
    }

    public static Plugin redocPlugin() {
        ReDocConfiguration configuration = new ReDocConfiguration();
        configuration.setUiPath("/redoc-ui");
        return new ReDocPlugin(configuration);
    }

    private ApiConfiguration() {}
}
