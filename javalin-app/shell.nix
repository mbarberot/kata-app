with (import <nixpkgs> {});

mkShell {
    buildInputs = [
        jdk17
        maven
        nodejs-18_x
    ];

    shellHook = ''
        export JAVA_HOME=${jdk17.home}
    '';
}